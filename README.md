
# java-starter

Simple maven Java Starter project with checkstyle configured for **Static Code Analysis**.

From root level, execute any of the below commands:

* `mvn clean install`
* `mvn checkstyle:check`
* `mvn pmd:check`
